const Page = require('../models/page');

exports.pageAddGet = function(req,res){
    res.render('page')
};
exports.pageAddPost = function (req,res) {
    console.log(req.body);
    const { pagename , accesstoken}=req.body;
    Page.findOne({accesstoken : accesstoken},(err, page) => {
        if(err) return console.log(err+" access da ton tai");
        else{
            const page = new Page({
                pagename , 
                accesstoken
            });
            page.save(err => {
                if(err) return console.log("loi khong them duoc ban ghi");
                res.redirect('/dashboard')
            })
        }
    })
};
exports.pageUpdateGet = function(req,res) {
    Page.findOne({_id : "5cf26ba132a176090c312942"},(err,result) => {
        if(err){
            return console.log(err);
        }
        try{
            res.render('updatepage',{
                pagename: result.pagename,
                accesstoken: result.accesstoken
            })
        }catch(error){
            console.log(loi+" "+error);
        };
    })
  
};
exports.pageUpdatePost = function(req,res) {
    Page.findByIdAndUpdate('5cf26ba132a176090c312942',req.body,{new : true}, (err,result) => {
        if(err) return console.log('khong update duoc');
        console.log(result);
        return res.redirect('/dashboard');
    })
}