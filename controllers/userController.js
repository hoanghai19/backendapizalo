const User = require('../models/user');
const bcrypt = require('bcryptjs');
const passport = require('passport');
exports.userLoginGet=function(req,res){
  console.log("client GET/LOGIN DA GOI LOGIN GET")
      // res.render('login')
   
    // res.json({
    //   "test":"tra du lieu ve cho clinet"
    // })
    
};
exports.userLoginPost=function(req,res,next){
  console.log("client POST/LOGIN/DA GOI LOGIN POST")
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash: true
      })(req, res, next);
      
};
exports.userRegisterGet=function (req,res) {
    res.render('register')
};
exports.userRegisterPost=function (req,res) { 
    const { name, username, password } = req.body;
    let errors = [];
  
    if (errors.length > 0) {
      res.render('register', {
        name,
        username,
        password,
      });
    } else {
      User.findOne({ username: username }).then(user => {
        if (user) {
          errors.push({ msg: 'Email already exists' });
          res.render('register', {
            name,
            username,
            password
          });
        } else {
          const newUser = new User({
            name,
            username,
            password
          });
  
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser
                .save()
                .then(user => {
                  res.redirect('/users/login');
                })
                .catch(err => console.log(err));
            });
          });
        }
      });
    }
};
exports.userLogoutGet=function(req,res){
    req.logout();
    res.redirect('/users/login');
};
exports.userChangepassGet=function(req,res){
    res.render('password')
};
exports.userChangepassPost=function(req,res){
    const user=req.user;
  bcrypt.compare(req.body.password, user.password, (err, isMatch) => {
    if (err) throw err;
    if (isMatch) {
     user.password=req.body.passwordNew;
    //  bcrypt
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) throw err;
        user.password = hash;
        user
          .save()
          .then(user => {
            res.redirect('/dashboard');
          })
          .catch(err => console.log("err not save pass to database"));
      });
    });
    } else {
      return done(null, false, { message: 'err not compare' });
    }
  });
}