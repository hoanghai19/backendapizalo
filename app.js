const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose=require('mongoose');
const bodyParser = require('body-parser');
const session=require('express-session');
const passport = require('passport');
const cors = require('cors');
const methodOverride=require('method-override');
// khai bao router
const indexRouter = require('./routes/index');
const userRouter=require('./routes/users')
const commentRouter=require('./routes/comments');
const pageRouter = require('./routes/page');



const app = express();
app.use(methodOverride('_method'));

// use cors port client x server
app.use(cors())
// Passport Config
require('./config/passport')(passport);
// mongoose config
const db=require('./config/keys.js').MongoURL;
//CONNECT TO MONGODB
mongoose.connect(db,{useNewUrlParser:true})
  .then(()=>console.log('ket noi thanh cong'))
  .catch(err=>console.log('ket noi that bai'))



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);
// Passport middleware khoi tao passport
app.use(passport.initialize());
app.use(passport.session());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// use router
app.use('/', indexRouter);
app.use('/users',userRouter);
app.use('/comments',commentRouter);
app.use('/page', pageRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
