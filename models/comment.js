const mongoose=require('mongoose');

const Schema=mongoose.Schema;
const blogComment=new Schema({
    comment: {
        type: String,
        required: true
    }
})
const Comment=mongoose.model('Comment',blogComment)
module.exports=Comment