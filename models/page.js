const mongoose = require('mongoose');
const Schema=mongoose.Schema;
const blogPage=new Schema({
    pagename: {
        type : String,
        required : true
    },
    accesstoken : {
        type : String,
        required : true
    }
})
const Page=mongoose.model('Page',blogPage)
module.exports=Page