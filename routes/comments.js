const express=require('express');
const router=express.Router();
const { forwardAuthenticated, ensureAuthenticated } = require('../config/auth');
const commentController=require('../controllers/commentController');

router.get('/content',ensureAuthenticated,commentController.commentDetailGet);
router.post('/content',commentController.commentDetailPost);
router.get('/update', ensureAuthenticated,commentController.commentUpdateGet);
router.put('/update',commentController.commentUpdatePut);

module.exports=router;