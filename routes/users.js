const express = require('express');
const router = express.Router();

//auth
const { forwardAuthenticated, ensureAuthenticated } = require('../config/auth');

// controller
const userController=require('../controllers/userController');

// Login Page chuyen den trang dashboard neu da dang nhap
router.get('/login', forwardAuthenticated, userController.userLoginGet);

// Login post
router.post('/login', userController.userLoginPost);

// Register Page chuyen den trang dashboard neu da dang nhap
router.get('/register', forwardAuthenticated, userController.userRegisterGet);

// Register post
router.post('/register', userController.userRegisterPost);

// Logout
router.get('/logout', userController.userLogoutGet);

// change password get
router.get('/change-password', ensureAuthenticated ,userController.userChangepassGet);

// change password post
router.post('/change-password',userController.userChangepassPost)

module.exports = router;
