const express=require('express');
const router=express.Router();
const pageCotroller = require('../controllers/pageController');
const { forwardAuthenticated, ensureAuthenticated } = require('../config/auth');

router.get('/add',ensureAuthenticated,pageCotroller.pageAddGet);
router.post('/add',pageCotroller.pageAddPost);
router.get('/update',ensureAuthenticated,pageCotroller.pageUpdateGet);
router.put('/update',pageCotroller.pageUpdatePost);


module.exports = router